<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;

class ItemController extends Controller
{
    //
    public function index(){
        $client = new Client();
        $booksResponse = $client->request('GET', 'http://localhost/provider1/books');
        $magazinesResponse = $client->request('GET', 'http://localhost/provider2/books');
        $provider3Response = $client->request('GET', 'http://localhost/provider3/books');
        var_dump($provider3Response);

        $books = json_decode($booksResponse->getBody(), true);
        $magazines = json_decode($magazinesResponse->getBody(), true);
        $provider3 = json_decode($provider3Response->getBody(), true);
        
        $items = array_merge($this->convertBooks($books), $this->convertMagazines($magazines), $this->convertBooks2($provider3));
        shuffle($items);
        return response()->json($items);
        return $items;

    }
    public function show($id){
        return response()->json([]);
    }

    private function convertBooks($books){
        $result = [];
        foreach ($books as $book){
            $item = [
                'numer_ksiazki' => $book['ID'],
                'tytul_ksiazki' => $book['Tytul'],
                'autor_ksiazki' => $book['Autor'],
				'wydawnictwo' => $book['Wydawnictwo'],
				'lczba_stron' => null,
				'data_wydania' => $book['Data wydania'],
				'rodzaj_okladki' => $book['Okladka'],
				'gatunek_literacki' => null,
				'isbn' => null,
            ];
            array_push($result, $item);
        }
        return $result;
    }
    private function convertMagazines($magazines){
        $result = [];
        foreach ($magazines as $magazine){
            $item = [
                'numer_ksiazki' => $magazine['ID'],
                'tytul_ksiazki' => $magazine['Tytul'],
                'autor_ksiazki' => $magazine['Autor'],
				'wydawnictwo' => $magazine['Wydawnictwo'],
				'lczba_stron' => $magazine['Strony'],
				'data_wydania' => null,
				'rodzaj_okladki' => null,
				'gatunek_literacki' => $magazine['Gatunek'],
				'isbn' => $magazine['ISBN'],
            ];
            array_push($result, $item);
        }
        return $result;
    }
 private function convertBooks2($books){
        $result = [];
        foreach ($books as $book){
            $item = [
                'numer_ksiazki' => $book['ID'],
                'tytul_ksiazki' => $book['Tytul'],
                'autor_ksiazki' => $book['Autor'],
				'wydawnictwo' => $book['Wydawnictwo'],
				'lczba_stron' => null,
				'data_wydania' => null,
				'rodzaj_okladki' => $book['Okladka'],
				'gatunek_literacki' => null,
				'isbn' => null,
            ];
            array_push($result, $item);
        }
        return $result;
    }
}